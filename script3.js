const loginInput = document.getElementById('login');
const passwordInput = document.getElementById('password');
const form = document.getElementById('login-form');

loginInput.addEventListener('copy', (event) => {
    event.preventDefault();
});

passwordInput.addEventListener('copy', (event) => {
    event.preventDefault();
});

form.addEventListener('submit', (event) => {
    event.preventDefault();

    const loginValue = loginInput.value.trim();
    const passwordValue = passwordInput.value.trim();
    if (loginValue==="") {
        alert('Введите логин!');
        return;
        }

    if (passwordValue==="") {
        alert('Введите пароль!');
        return;
    }

        const allowedCharacters = /^[a-zA-Z0-9._]+$/;
    if (!allowedCharacters.test(loginValue)) {
        alert('Логин может содержать только английские буквы, цифры и знаки: точка, нижнее подчеркивание');
        return;
    }

    if (!allowedCharacters.test(passwordValue)) {
        alert('Пароль может содержать только английские буквы, цифры и знаки: точка, нижнее подчеркивание');
        return;
    }

    console.log(`Логин: ${loginValue}, пароль: ${passwordValue}`);
    form.submit();
});