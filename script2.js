class GameField {
    constructor() {
        this.state = [[null, null, null], [null, null, null], [null, null, null]];
        this.mode = 'x';
        this.isOverGame = false;
        this.winnerMsg = "";
    }

    getGameFieldStatus() {
        return this.state;
    }

    setMode(newMode) {
        this.mode = newMode;
    }

    FieldCellValue(row, col) {
        if (this.state[row][col] !== null) {
            return false;
        }
        this.state[row][col] = this.mode;
        this.winnerMsg = this.checkGameField(this.state);
        if (this.winnerMsg !== "") {
            this.isOverGame = true;
        } else {
            if (this.mode === 'x') {
                this.mode = 'o';
            } else {
                this.mode = 'x';
            }
        }
        return true;
    }

    checkGameField(field) {
        let cs = ['x', 'o'];
        let isWinners = [false, false];
        for (let j = 0; j < 2; j++) {
            for (let i = 0; i < 3; i++) {
                if ((field[0][i] === cs[j] && field[1][i] === cs[j] && field[2][i] === cs[j]) ||
                    (field[i][0] === cs[j] && field[i][1] === cs[j] && field[i][2] === cs[j])) {
                    isWinners[j] = true;
                }
            }
            if ((field[0][0] === cs[j] && field[1][1] === cs[j] && field[2][2] === cs[j]) ||
                (field[0][2] === cs[j] && field[1][1] === cs[j] && field[2][0] === cs[j])) {
                isWinners[j] = true;
            }
        }

        let isFieldFull = true;
        for (let i = 0; i < 3; i++) {
            for (let j = 0; j < 3; j++) {
                if (field[i][j] === null) {
                    isFieldFull = false;
                }
            }
        }

        let msg = "";
        if (isWinners[0] === true && isWinners[1] === true ||
            (isFieldFull && isWinners[0] === false && isWinners[1] === false)) {
            msg = 'Ничья!';
        } else if (isWinners[0] === true && isWinners[1] === false) {
            msg = 'Крестики победили!';
        } else if (isWinners[0] === false && isWinners[1] === true) {
            msg = 'Нолики победили!';
        }
        return msg;
    }

    getMode() {
        return this.mode;
    }

    getWinnerMsg() {
        return this.winnerMsg;
    }

}

const gameField = new GameField();

while (!gameField.isOverGame) {
    console.log(gameField.getGameFieldStatus());
    console.log('Сейчас ходит ' + gameField.getMode() + ':');
    const row = parseInt(prompt('Введите номер строки (0-2): '));
    const col = parseInt(prompt('Введите номер столбца (0-2): '));

    if (!isNaN(row) && !isNaN(col) && row < 3 && row >= 0 && col < 3 && col >= 0) {
        if (!gameField.FieldCellValue(row, col)) {
            alert('Эта клетка уже занята!');
        }
    } else {
        alert('Неверный ввод!');
    }
}

console.log(gameField.getGameFieldStatus());
console.log(gameField.getWinnerMsg());