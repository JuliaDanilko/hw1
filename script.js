const gameField = [
    ['x', 'o', null],
    ['x', null, 'o'],
    ['x', 'o', 'o']
];

function checkGameField(field) {
    let cs = ['x', 'o'];
    let isWinners = [false, false];
    for (let j = 0; j < 2; j++) {
        for (let i = 0; i < 3; i++) {
            if ((field[0][i] === cs[j] && field[1][i] === cs[j] && field[2][i] === cs[j]) ||
                (field[i][0] === cs[j] && field[i][1] === cs[j] && field[i][2] === cs[j])) {
                isWinners[j] = true;
            }
        }
        if ((field[0][0] === cs[j] && field[1][1] === cs[j] && field[2][2] === cs[j]) ||
            (field[0][2] === cs[j] && field[1][1] === cs[j] && field[2][0] === cs[j])) {
            isWinners[j] = true;
        }
    }

    let msg = "";
    if (isWinners[0] === true && isWinners[1] === true) {
        msg = "Ничья";
    } else if (isWinners[0] === true && isWinners[1] === false) {
        msg = "Крестики победили";
    } else if (isWinners[0] === false && isWinners[1] === true) {
        msg = "Нолики победили";
    }
    return msg;
}

let msg = checkGameField(gameField);
console.log(msg);
